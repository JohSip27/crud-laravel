<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\castmodel;

class CastController extends Controller
{
    public function index()
        {
            $kategori = castmodel::all();
            return view ('tampil' , compact('kategori'));
        }

    public function create()
    {
           return view('tambah');
    }
    public function insert(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        castmodel::create($request->all());
        return redirect('/');
    }
    public function detail($id)
    {
        $value = castmodel::find($id);
        return view('detail' , compact('value'));
    }
    public function edit($id)
    {
        $value = castmodel::find($id);
        return view('edit' , compact('value'));
    }
    public function update(Request $request , $id)
    {
        $data = castmodel::find($id);
        $data->update($request->all());
        return redirect ('/')->with('success');
    }
    public function delete($id)
    {
        $value = castmodel::find($id);
        $value->delete();
        return redirect ('/');

    }
}
