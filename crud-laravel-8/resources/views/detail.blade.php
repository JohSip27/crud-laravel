<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body class="bg-warning">
    <h1 class="text-center mt-3">Detail Pemain Film</h1>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-8">
                <div class="card">
                    <div class="card-body">
                    <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label"><b>Nama<b></label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nama" value="{{ $value->nama }}">
                  </div>
                        <br>
                        <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label"><b>Umur<b></label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nama" value="{{ $value->umur }}">
                      </div>
                        <br>
                        <label for="bio">Bio</label>
                        <textarea class="form-control"id="floatingTextarea2" style="height: 330px" name="bio">{{ $value->bio }}</textarea>
                    </div>
                    <a href="\" class="btn btn-primary m-1 p-2">Kembali</a>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html>