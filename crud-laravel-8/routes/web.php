<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/' , [CastController::class , 'index'])->name('/');
Route::get('/tambah' , [CastController::class , 'create'])->name('create');
Route::post('/insert' , [CastController::class , 'insert'])->name('insert');
Route::get('/detail/{id}' , [CastController::class , 'detail'])->name('detail');
Route::get('/edit/{id}' , [CastController::class , 'edit'])->name('edit');
Route::post('/update/{id}' , [CastController::class , 'update'])->name('update');
Route::get('/delete/{id}' , [CastController::class , 'delete'])->name('delete');
